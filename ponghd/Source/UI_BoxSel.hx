package ;
import openfl.display.Sprite;
import openfl.filters.BlurFilter;

// UI_BoxSel
// Graphics for blue boxes (I didn't know how to change colors properly, so little derpy thing)

class UI_BoxSel extends Sprite
{

	public function new() 
	{
		super();
		this.graphics.beginFill(0x58ACFA);
		this.graphics.drawRoundRect(0, 0, 240, 360, 10, 10);
		this.graphics.endFill();
	}
	
}