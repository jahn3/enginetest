package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.display.Sprite;
import motion.Actuate;
import motion.easing.*;
import openfl.text.TextField;

// Pause
// Overlay to display pause menu

class Pause extends Sprite 
{   
    // graphics
    private var box1:UI_Box;
    private var box2:UI_Box;
    
    private var box1sel:UI_BoxSel;
    
    // text
    private var mode1:TextField;
    private var mode2:TextField;
    
    private var desr1:TextField;
    
    // selected box
    private var m:Int;
    
    // status
    private var status:Main.Status;
    
    // yes, i reused ModeSelect so I can save time
    
	public function new() 
	{
		super();
        
        // game is now Paused and animation is playing
        status = Wait;
        Main.gameStatus = Pause;
        m = 1;
        
        // graphics
        box1 = new UI_Box();
        box1.x = 640-240-36;
        box1.y = 180;
        box1.alpha = 0;
        this.addChild(box1);
        
        box2 = new UI_Box();
        box2.x = 640+36;
        box2.y = 180;
        box2.alpha = 0;
        this.addChild(box2);
        
        // blue box
        box1sel = new UI_BoxSel();
        box1sel.x = 640+36;
        box1sel.y = 180;
        box1sel.alpha = 0;
        this.addChild(box1sel);
        
        // texts
        mode1 = new TextField();
        addChild(mode1);
        mode1.width = box1.width;
        mode1.height = box1.height;
        mode1.x = box1.x;
        mode1.y = box1.y + 16;
        mode1.alpha = 0;
        mode1.defaultTextFormat = Fonts.modeText;
        mode1.selectable = false;
        mode1.text = "Resume";
        
        mode2 = new TextField();
        addChild(mode2);
        mode2.width = box2.width;
        mode2.height = box2.height;
        mode2.x = box2.x;
        mode2.y = box2.y + 16;
        mode2.alpha = 0;
        mode2.defaultTextFormat = Fonts.modeText;
        mode2.selectable = false;
        mode2.text = "Quit";
        
        desr1 = new TextField();
        addChild(desr1);
        desr1.width = box1.width * 2 + 32;
        desr1.height = 96;
        desr1.x = 640 - desr1.width / 2;
        desr1.y = box1.y + box2.height + 16;
        desr1.alpha = 0;
        desr1.defaultTextFormat = Fonts.desrText;
        desr1.selectable = false;
        desr1.text = "Enter - Select\nLeft & Right - Change Mode\nEsc - Resume";
        
        // animation
        Actuate.tween (box1, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6);
        Actuate.tween (box2, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6);
        Actuate.tween (mode1, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6);
        Actuate.tween (mode2, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6);
        Actuate.tween (desr1, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6).onComplete(statusDone);
        
        // every frame event listener
        this.addEventListener(Event.ENTER_FRAME, everyFrame);
	}
    
    private function everyFrame(event:Event):Void {
        if (status == Done) {
            // show blue box
            Actuate.tween (box1sel, 1, { alpha: 1 }).ease(Expo.easeOut);
            if (Kinput.keys[37] || Kinput.keys[27]) m = 1; // Resume
            if (Kinput.keys[39]) m = 2; // Quit
            if (m == 1) {
                box1sel.x = box1.x;
                if (Kinput.keys[13]) modeSelected();
            }
            if (m == 2) {
                box1sel.x = box2.x;
                if (Kinput.keys[13]) modeSelected();
            }
            if (Kinput.keys[27]) modeSelected(); // Resume (escape key)
        }
    }
    
    private function statusDone() {
        status = Done;
    }
    
    private function modeSelected() {
        status = Wait;
        if (m == 1) {
            trace("Resuming...");
        }
        if (m == 2) {
            trace("Quitting...");
        }
        
        // animate out
        Actuate.tween (box1, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (box2, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (box1sel, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (mode1, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (mode2, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (desr1, 1, { alpha: 0 }).ease(Expo.easeOut).onComplete(switchToGame);
    }
    
    private function switchToGame() {
        if (m == 1) {
            Main.gameStatus = InGame;
            Game.inGameStatus = Playing;
            Game.initialTime = Lib.getTimer();
        }
        if (m == 2) {
            Main.gameStatus = InGame;
            Game.inGameStatus = Quit;
        } 
    }
    
}