package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.Assets;
import motion.Actuate;
import motion.easing.*;

// Intro video
// WIP

class Intro extends Sprite
{
    private var text1:TextField;
    
    public function new() 
	{
		super();
        
        // gameStatus is now Intro
        Main.gameStatus = Intro;
		
        text1 = new TextField();
        addChild(text1);
        text1.width = 1280;
        text1.height = 640;
        text1.y = 360-32;
        text1.alpha = 0;
        text1.defaultTextFormat = Fonts.introText;
        text1.selectable = false;
        text1.text = "Starpixel Studios Presents";
        
        // switch to menu
        Actuate.tween (text1, 2, { alpha: 1 }).repeat(1).reflect().onComplete(Main.toMenu);
	}
	
}