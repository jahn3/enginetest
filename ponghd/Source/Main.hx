package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.Assets;
import openfl.events.KeyboardEvent;

// Main file
// Initialize classes and switch scenes

enum Status { // animation status
    Wait;
    Done;
}

enum GameStatus { // game status
    Intro;
    Menu;
    ModeSelect;
    InGame;
    Main;
    Pause;
}

class Main extends Sprite 
{
	var inited:Bool;
    
    private static var currentScreen:Sprite; // current screen being displayed
    
    public static var gameStatus:GameStatus; // global game status
    
	/* ENTRY POINT */
	
	function resize(e) 
	{
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		if (inited) return;
		inited = true;
        
        // game is now Main
        gameStatus = Main;
		
		// initialize classes
        new Kinput(); // keyboard input
        new Fonts(); // font library
        
        // add keyboard listeners
        stage.addEventListener(KeyboardEvent.KEY_DOWN, Kinput.onKeyDown);
        stage.addEventListener(KeyboardEvent.KEY_UP, Kinput.onKeyUp);
        
        // load in first screen. you can change it to whatever for debugging purposes.
        currentScreen = new Intro();
        Lib.current.addChild(currentScreen);
	}
    
    /* Screen Changes */
    public static function toGame() {
        Lib.current.removeChild(currentScreen); 
        
        currentScreen = new Game();
        Lib.current.addChild(currentScreen); 
    }
    
    public static function toMenu() {
        Lib.current.removeChild(currentScreen); 
        
        currentScreen = new Menu();
        Lib.current.addChild(currentScreen); 
    }
    
    public static function toIntro() {
        Lib.current.removeChild(currentScreen); 
        
        currentScreen = new Intro();
        Lib.current.addChild(currentScreen); 
    }
    
	/* SETUP */
    // don't touch

	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, added);
	}

	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
		//
	}
}