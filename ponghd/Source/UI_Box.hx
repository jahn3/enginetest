package ;
import openfl.display.Sprite;
import openfl.filters.BlurFilter;

// UI_Box
// Graphics for UI Boxes

class UI_Box extends Sprite
{

	public function new() 
	{
		super();
		this.graphics.beginFill(0xEEEEEE);
		this.graphics.drawRoundRect(0, 0, 240, 360, 10, 10);
		this.graphics.endFill();
	}
	
}