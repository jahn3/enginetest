package ;
import openfl.display.Sprite;

// Platform
// Graphics for platforms

class Platform extends Sprite
{

	public function new() 
	{
		super();
		this.graphics.beginFill(0x666666);
		this.graphics.drawRect(0, 0, 20, 180);
		this.graphics.endFill();
	}
	
}