package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.Assets;

// Fonts
// Load in fonts and TextFormats

class Fonts extends Sprite
{
    public static var introText:TextFormat;
    public static var titleText:TextFormat;
    public static var subtitleText:TextFormat;
    public static var modeText:TextFormat;
    public static var desrText:TextFormat;
    public static var desrTextDiff:TextFormat;
    public static var desrTextDiffE:TextFormat;
    public static var scoreText:TextFormat;
    public static var scoreTextEffect:TextFormat;
    
    private var font1 = Assets.getFont("fonts/GeosansLight.ttf").fontName;
    
	public function new() 
	{
		super();
        
        introText = new TextFormat(font1, 64, 0x000000, true);
        introText.align = TextFormatAlign.CENTER;
        
        titleText = new TextFormat(font1, 128, 0x000000, true);
        titleText.align = TextFormatAlign.CENTER;
        
        subtitleText = new TextFormat(font1, 36, 0x000000, true);
        subtitleText.align = TextFormatAlign.CENTER;
        
        modeText = new TextFormat(font1, 36, 0x666666, true);
        modeText.align = TextFormatAlign.CENTER;
        
        desrText = new TextFormat(font1, 18, 0x666666, true);
        desrText.align = TextFormatAlign.CENTER;
        
        desrTextDiff = new TextFormat(font1, 128, 0xDDDDDD, true);
        desrTextDiff.align = TextFormatAlign.CENTER;
        
        desrTextDiffE = new TextFormat(font1, 128, 0x58ACFA, true);
        desrTextDiffE.align = TextFormatAlign.CENTER;
        
        scoreText = new TextFormat(font1, 300, 0xDDDDDD, true);
        scoreText.align = TextFormatAlign.CENTER;
        
        scoreTextEffect = new TextFormat(font1, 300, 0x58ACFA, true);
        scoreTextEffect.align = TextFormatAlign.CENTER;
	}
	
}