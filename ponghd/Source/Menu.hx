package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.display.Sprite;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.Assets;
import motion.Actuate;
import motion.easing.*;
import openfl.events.KeyboardEvent;

// Menu
// (actually it's a title screen)

class Menu extends Sprite 
{
    private var title:TextField;
    private var play:TextField;
    
    private var button:MenuButton; // temporary. not used
    
    private var status:Main.Status; // animation status for Menu

	public function new() 
	{
		super();
        
        // animation is playing
        status = Wait;
        
        // game is now Menu
        Main.gameStatus = Menu;

        title = new TextField();
        addChild(title);
        title.width = 1280;
        title.height = 640;
        title.y = 240;
        title.alpha = 0;
        title.defaultTextFormat = Fonts.titleText;
        title.selectable = false;
        title.text = "PONGHD";
        
        play = new TextField();
        addChild(play);
        play.width = 1280;
        play.y = 480;
        play.alpha = 0;
        play.defaultTextFormat = Fonts.subtitleText;
        play.selectable = false;
        play.text = "Press Enter to Start";
        
        Actuate.tween (title, 10, { alpha: 1 });
        Actuate.tween (play, 2, { alpha: 1 }).delay(1);
        
        // animation is done
        Actuate.timer (2).onComplete(statusDone);
        
        // every frame event listener
        this.addEventListener(Event.ENTER_FRAME, everyFrame);
	}
    
    private function everyFrame(event:Event):Void {
        if (status == Done && Kinput.keys[13]) { // press enter to start
            Actuate.tween (title, 1, { alpha: 0 } );
            Actuate.tween (play, 1, { alpha: 0 } );
            Actuate.timer (1).onComplete(Main.toGame); // switch to Game
        }
    }
    
    private function statusDone() {
        status = Done;
    }
    
}