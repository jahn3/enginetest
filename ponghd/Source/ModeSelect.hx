package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.display.Sprite;
import motion.Actuate;
import motion.easing.*;
import openfl.text.TextField;

// ModeSelect
// Overlay to select mode

enum ModeStatus {
    Player;
    Difficulty;
    HowTo;
    Wait;
    Done;
}

class ModeSelect extends Sprite 
{   
    // graphics
    private var box1:UI_Box;
    private var box2:UI_Box;
    
    private var box1sel:UI_BoxSel;
    
    // texts
    private var mode1:TextField;
    private var mode2:TextField;
    
    private var desr1:TextField;
    private var desr2:TextField;
    
    // game mode
    private var m:Int;
    
    // status
    private var status:Main.Status;
    private var modeStatus:ModeStatus;
    
    // difficulty
    private var diff:Int;
    
	public function new() 
	{
		super();
        
        Main.gameStatus = ModeSelect;
        modeStatus = Wait;
        m = 1;
        diff = 3;
        
        // graphics
        box1 = new UI_Box();
        box1.x = 640-240-36;
        box1.y = 180;
        box1.alpha = 0;
        this.addChild(box1);
        
        box2 = new UI_Box();
        box2.x = 640+36;
        box2.y = 180;
        box2.alpha = 0;
        this.addChild(box2);
        
        // blue box
        box1sel = new UI_BoxSel();
        box1sel.x = 640+36;
        box1sel.y = 180;
        box1sel.alpha = 0;
        this.addChild(box1sel);
        
        // text
        mode1 = new TextField();
        addChild(mode1);
        mode1.width = box1.width;
        mode1.height = box1.height;
        mode1.x = box1.x;
        mode1.y = box1.y + 16;
        mode1.alpha = 0;
        mode1.defaultTextFormat = Fonts.modeText;
        mode1.selectable = false;
        mode1.text = "Single Player";
        
        mode2 = new TextField();
        addChild(mode2);
        mode2.width = box2.width;
        mode2.height = box2.height;
        mode2.x = box2.x;
        mode2.y = box2.y + 16;
        mode2.alpha = 0;
        mode2.defaultTextFormat = Fonts.modeText;
        mode2.selectable = false;
        mode2.text = "Multi Player";
        
        desr1 = new TextField();
        addChild(desr1);
        desr1.width = box1.width;
        desr1.height = box1.height;
        desr1.x = 640 - desr1.width / 2;
        desr1.y = box1.y + box2.height + 16;
        desr1.alpha = 0;
        desr1.defaultTextFormat = Fonts.desrText;
        desr1.selectable = false;
        desr1.text = "Enter - Select\nLeft & Right - Change Mode";
        
        desr2 = new TextField();
        addChild(desr2);
        desr2.width = box1.width;
        desr2.height = box1.height;
        desr2.x = 640 - desr1.width / 2;
        desr2.y = 360-64;
        desr2.alpha = 0;
        desr2.defaultTextFormat = Fonts.desrTextDiffE;
        desr2.selectable = false;
        desr2.text = "NM";
        
        // animation
        Actuate.tween (box1, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6);
        Actuate.tween (box2, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6);
        Actuate.tween (mode1, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6);
        Actuate.tween (mode2, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6);
        Actuate.tween (desr1, 1, { alpha: 1 }).ease(Expo.easeOut).delay(0.6).onComplete(statusDone, [Player]);
        
        // every frame event listener
        this.addEventListener(Event.ENTER_FRAME, everyFrame);
	}
    
    private function everyFrame(event:Event):Void {
        if (modeStatus == Player) {
            // show blue box
            Actuate.tween (box1sel, 1, { alpha: 1 }).ease(Expo.easeOut);
            if (Kinput.keys[37]) m = 1; // choose single player
            if (Kinput.keys[39]) m = 2; // choose multi player
            if (m == 1) {
                box1sel.x = box1.x;
                if (Kinput.keys[13]) modeSelected(); // select single player
            }
            if (m == 2) {
                box1sel.x = box2.x;
                if (Kinput.keys[13]) modeSelected(); // select multi player
            }
            if (Kinput.keys[80] && Kinput.keys[17]) { // ctrl+p
                m = 99;
                modeSelected();
            }   
        }
        if (modeStatus == Difficulty) { // choose difficulty
            if (Kinput.keys[37]) {
                statusDone(Wait);
                diff++;
                diff = (diff > 5)?(5):(diff);
                Actuate.tween (desr2, 0.2, { alpha: 1 }).ease(Expo.easeOut).repeat(1).reflect().onComplete(statusDone, [Difficulty]);
            }
            if (Kinput.keys[39]) {
                statusDone(Wait);
                diff--;
                diff = (diff < 3)?(3):(diff);
                Actuate.tween (desr2, 0.2, { alpha: 1 }).ease(Expo.easeOut).repeat(1).reflect().onComplete(statusDone, [Difficulty]);
            }
            if (diff == 5) {
                desr1.text = "EZ";
                desr2.text = desr1.text;
            }
            if (diff == 4) {
                desr1.text = "NM";
                desr2.text = desr1.text;
            }
            if (diff == 3) {
                desr1.text = "HD";
                desr2.text = desr1.text;
            }
            if (Kinput.keys[13]) { // select difficulty
                Game.AIDiff = diff;
                statusDone(Wait);
                Actuate.tween (box1, 1, { alpha: 0 }).ease(Expo.easeOut).delay(0.4);
                Actuate.tween (mode1, 1, { alpha: 0 }).ease(Expo.easeOut).delay(0.4);
                Actuate.tween (desr2, 1, { alpha: 0 }).ease(Expo.easeOut).delay(0.4);
                Actuate.tween (desr1, 1, { alpha: 0 }).ease(Expo.easeOut).delay(0.4);
                Actuate.tween (desr2, 0.2, { alpha: 1 }).ease(Expo.easeOut).repeat(1).reflect();
                Actuate.timer (1.2).onComplete(switchInGame);
            }
        }
    }
        
    // mode selected
    private function modeSelected() {
        statusDone(Wait);
        Game.gameMode = m;
        trace("Mode " + Game.gameMode);
        
        
        Actuate.tween (box1, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (box2, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (box1sel, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (mode1, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (mode2, 1, { alpha: 0 }).ease(Expo.easeOut);
        Actuate.tween (desr1, 1, { alpha: 0 }).ease(Expo.easeOut).onComplete(difficulty);
    }
    
    // choose difficulty. if game mode is not 1 then skip
    private function difficulty() {
        if (m == 1) {
            diff = 4;
            
            box1.x = 640-120;
            box1.y = 180;

            mode1.x = box1.x;
            mode1.y = box1.y + 16;
            mode1.text = "Difficulty";
            
            desr1.y = box1.y;
            desr1.defaultTextFormat = Fonts.desrTextDiff;
            desr1.y = desr2.y;
            desr1.text = desr2.text;
            
            Actuate.tween (box1, 1, { alpha: 1 }).ease(Expo.easeOut);
            Actuate.tween (mode1, 1, { alpha: 1 }).ease(Expo.easeOut);
            Actuate.tween (desr1, 1, { alpha: 1 }).ease(Expo.easeOut);
            Actuate.tween (desr2, 0.2, { alpha: 1 }).ease(Expo.easeOut).repeat(1).reflect().onComplete(statusDone, [Difficulty]);
            
        } else {
            switchInGame();
        }
    }
    
    /*
    
    TO DO: ADD IN HOW TO PLAY

    */
    
    // mode status
    private function statusDone(s:ModeStatus) {
        modeStatus = s;
    }
    
    // back to Game
    private function switchInGame() {
        trace("Starting Game...");
        Main.gameStatus = InGame;   
    }
    
}