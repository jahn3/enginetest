package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.events.KeyboardEvent;

// Kinput
// Detect keyboard inputs and store key arrays

class Kinput extends Sprite 
{   
    public static var keys:Array<Bool>;
    
	function new() 
	{
        super();
		// key arrays
        keys = [];
	}
    
    public static function onKeyDown(evt:KeyboardEvent):Void {
	    keys[evt.keyCode] = true;
        trace("Char code: " + evt.charCode);
	    trace("Key code: " + evt.keyCode);
    }

    public static function onKeyUp(evt:KeyboardEvent):Void {
        keys[evt.keyCode] = false;
        trace("Char code: " + evt.charCode);
	    trace("Key code: " + evt.keyCode);
    }
}