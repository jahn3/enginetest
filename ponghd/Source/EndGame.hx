package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.display.Sprite;
import motion.Actuate;
import motion.easing.*;
import openfl.text.TextField;

class EndGame extends Sprite 
{   
    private var desr1:TextField;
    
    private var status:Main.Status;
    
	public function new() 
	{
		super();
        
        status = Wait;
        Main.gameStatus = Pause;
        
        desr1 = new TextField();
        addChild(desr1);
        desr1.width = 640;
        desr1.height = 360;
        desr1.x = 640-desr1.width/2;
        desr1.y = 720-64;
        desr1.alpha = 0;
        desr1.defaultTextFormat = Fonts.subtitleText;
        desr1.selectable = false;
        desr1.text = "Press Esc to Return";
        
        Actuate.tween (desr1, 1, { alpha: 1 }).ease(Expo.easeOut).onComplete(statusDone);
        
        // every frame event listener
        this.addEventListener(Event.ENTER_FRAME, everyFrame);
	}
    
    private function everyFrame(event:Event):Void {
        if (status == Done) {
            if (Kinput.keys[27]) switchToGame();
        }
    }
    
    private function statusDone() {
        status = Done;
    }
    
    private function switchToGame() {
        Main.gameStatus = InGame;
        Game.inGameStatus = Quit; 
    }
    
}