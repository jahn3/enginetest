package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.display.Sprite;
import motion.Actuate;
import motion.easing.*;
import openfl.geom.Point;
import openfl.text.TextField;

// Game
// contains the main game
/*

TO DO: BALANCE THE DIFFICULTY AND ADD IN UNLOCKABLE SHD MODE (SuperHard)

*/

enum InGameStatus { // in-game status
    Paused;
    Playing;
    NotStarted;
    Winner;
    Quit;
}

class Game extends Sprite 
{
    // overlays
    private static var overlay:Sprite;
    
    // game status
    public static var inGameStatus:InGameStatus;
    
    // game graphics
    private var platform1:Platform;
    private var platform2:Platform;
    private var ball:Ball;
    
    // game values 
    public static var gameMode:Int; // 1P or 2P
    private var platformSpeed:Int;
    private var ballMovement:Point;
    private var ballSpeedDef:Int; // default ballSpeed
    private var ballSpeed:Float; // current ballSpeed
    private var ballSpeedMod:Float; // ballSpeed modifier (WIP). want to change the ball speed over time
    private var AIMoveThreshold:Float; // how fast AI respond to the ball. change below two values to change this
    private var AIMoveThresholdMod:Int; // This Value over...
    private var AIMoveThresholdDiv:Int; // This.
    public static var AIDiff:Int; // how slow does the AI move. higher the slower.
    private var winCondition:Int; // wonned at this point
    
    // game text
    private var scoreP1:Int;
    private var scoreP2:Int;
    private var s1:TextField;
    private var s2:TextField;
    private var sE:TextField; // score effect when they score
    private var cE:TextField; // you're winner!
    private var desr1:TextField; // press escape to return
    
    // time stuffs
    public static var initialTime:Int; // time when the game reset
    private var deltaTime:Int; // currenttime - initialtime (time elapsed since the reset)
    private var previousTime:Int; // time when the game paused and resumed
    private var sec:Float; // WIP
    private var cur:Float; // WIP
    
	public function new() 
	{
		super();	
        
        // change game values here:
        platformSpeed = 30;
        ballSpeedDef = 20;
        ballSpeedMod = 1.5;
        AIDiff = 4;
        AIMoveThresholdMod = 1; // lower the more responsive. don't go below 0 and over the Div.
        AIMoveThresholdDiv = 10;
        AIMoveThreshold = 1280 / AIMoveThresholdDiv * AIMoveThresholdMod; // don't change this
        winCondition = 15;
        
        // initializing
        ballSpeed = ballSpeedDef;
        ballMovement = new Point(0, 0);
        
        // in-game is now NotStarted
        inGameStatus = NotStarted;
        
        scoreP1 = 0;
        scoreP2 = 0;
        
        // score texts
        s1 = new TextField();
        addChild(s1);
        s1.width = 640;
        s1.height = 340;
        s1.x = 0;
        s1.y = 360-s1.height/2;
        s1.alpha = 0;
        s1.defaultTextFormat = Fonts.scoreText;
        s1.selectable = false;
        s1.text = Std.string(scoreP1);
        
        s2 = new TextField();
        addChild(s2);
        s2.width = 640;
        s2.height = 340;
        s2.x = 640-20;
        s2.y = 360-s2.height/2;
        s2.alpha = 0;
        s2.defaultTextFormat = Fonts.scoreText;
        s2.selectable = false;
        s2.text = Std.string(scoreP2);
        
        sE = new TextField();
        addChild(sE);
        sE.width = 640;
        sE.height = 340;
        sE.x = 640-20;
        sE.y = 360-s2.height/2;
        sE.alpha = 0;
        sE.defaultTextFormat = Fonts.scoreTextEffect;
        sE.selectable = false;
        sE.text = "0";
        
        // used for countdown and endgame
        cE = new TextField();
        addChild(cE);
        cE.width = 640;
        cE.height = 360;
        cE.x = 640-cE.width/2;
        cE.y = 360-64;
        cE.alpha = 0;
        cE.defaultTextFormat = Fonts.desrTextDiffE;
        cE.selectable = false;
        cE.text = "READY";
        
        // graphics
		platform1 = new Platform();
        platform1.x = 0 + 64;
        platform1.y = 720;
        this.addChild(platform1);
        
        platform2 = new Platform();
        platform2.x = 1280 - 64 - (platform2.width);
        platform2.y = 720;
        this.addChild(platform2);

        ball = new Ball();
        ball.x = 640 - (ball.width / 2);
        ball.y = 720;
        this.addChild(ball);
        
        // graphics animation
        Actuate.tween (platform1, 1, { y: 360 - (platform1.height / 2) }).ease(Expo.easeOut);
        Actuate.tween (platform2, 1, { y: 360 - (platform1.height / 2) }).ease(Expo.easeOut).delay(0.2);
        Actuate.tween (ball, 1, { y: 360 - (ball.height / 2) }).ease(Expo.easeOut).delay(0.4);
        
        // open overlay ModeSelect
        Actuate.timer (0.6).onComplete(openModeSelect);
        
        // every frame event listener
        this.addEventListener(Event.ENTER_FRAME, everyFrame);
	}
    
    // this is where the actual game is coded
    private function everyFrame(event:Event):Void {
        // check is gameStatus is InGame
        if (Main.gameStatus == InGame) {
            closeWindow(); // close overlays
            
            if (inGameStatus == NotStarted) { // start game
                countdown();
            }
            if (inGameStatus == Playing) { // playing game
                // InGame
                
                // Single Player
                if (gameMode == 1) {
                    // P1
                    if (Kinput.keys[87]) platform1.y -= platformSpeed;
                    if (Kinput.keys[83]) platform1.y += platformSpeed;
                    
                    // AI platform movement
                    if (ball.x > AIMoveThreshold && ball.y > platform2.y + 70) {
                        platform2.y += platformSpeed/AIDiff;
                    }
                    if (ball.x > AIMoveThreshold && ball.y < platform2.y + 30) {
                        platform2.y -= platformSpeed/AIDiff;
                    }
                }
                
                // Multi Player
                if (gameMode == 2) {
                    if (Kinput.keys[87]) platform1.y -= platformSpeed;
                    if (Kinput.keys[83]) platform1.y += platformSpeed;

                    if (Kinput.keys[38]) platform2.y -= platformSpeed;
                    if (Kinput.keys[40]) platform2.y += platformSpeed;
                
                }
                
                // Debug Mode (Enter Ctrl+P at the ModeSelect to Enter)
                if (gameMode == 99) {
                    // EVERYONE IS A BOT
                    if (ball.x > AIMoveThreshold && ball.y > platform1.y + 70) {
                        platform1.y += platformSpeed;
                    }
                    if (ball.x > AIMoveThreshold && ball.y < platform1.y + 30) {
                        platform1.y -= platformSpeed;
                    }
                    
                    if (ball.x > AIMoveThreshold && ball.y > platform2.y + 70) {
                        platform2.y += platformSpeed;
                    }
                    if (ball.x > AIMoveThreshold && ball.y < platform2.y + 30) {
                        platform2.y -= platformSpeed;
                    }
                }
                
                if (platform1.y >= 720-platform1.height) platform1.y = 720-platform1.height;
                if (platform1.y <= 0) platform1.y = 0;
                if (platform2.y >= 720-platform2.height) platform2.y = 720-platform2.height;
                if (platform2.y <= 0) platform2.y = 0;
                
                // update ball movement
                ball.x += ballMovement.x;
                ball.y += ballMovement.y;
                
                // ball platform bounce
                if (ballMovement.x < 0 && ball.x < 64+platform1.width && ball.y >= platform1.y && ball.y <= platform1.y+platform1.height) {
                    bounceBall(); // change direction
                    ball.x = 64+platform1.width;
                }
                if (ballMovement.x > 0 && ball.x > 1280-64-platform2.width-ball.width && ball.y >= platform2.y && ball.y <= platform2.y+platform2.height) {
                    bounceBall(); // change direction
                    ball.x = 1280-64-platform2.width-ball.width;
                }
                
                // top and bottom bound bounce
                if (ball.y < 0 || ball.y > 720-ball.height) ballMovement.y *= -1;
                
                // you scored!
                if (ball.x < 0) winGame(2);
                if (ball.x > 1280-ball.width) winGame(1);
                
                // open pause menu
                if (Kinput.keys[27]) openPause();
                
                /* WIP
                deltaTime = (Lib.getTimer() - initialTime) + previousTime;
                
                sec = deltaTime / 1000;
                
                if (sec > 20) {
                    ballSpeed
                }
                */
                
            }
            if (inGameStatus == Quit) { // I QUIT
                Main.gameStatus = ModeSelect;
                inGameStatus = NotStarted;
                Main.toGame();
            }
            
        }
    }
    
    private function bounceBall():Void {
		var direction:Int = (ballMovement.x > 0)?( -1):(1);
		var randomAngle:Float = (Math.random() * Math.PI / 2) - 45;
		ballMovement.x = direction * Math.cos(randomAngle) * ballSpeed;
		ballMovement.y = Math.sin(randomAngle) * ballSpeed;
	}
    
    public function setGameStatus(gs:InGameStatus) {
        inGameStatus = gs;
        trace ("Game Status: " + gs);
        
        if (gs == Playing) {
            reset();
        }
    }
    
    private function countdown() {
        setGameStatus(Paused);
        
        s1.alpha = 1;
        s2.alpha = 1;
        
        Actuate.tween (cE, 1, { alpha: 1 }).repeat(1).reflect().onComplete(setGameStatus, [Playing]);
    }
    
    private function winGame(player:Int):Void {
    if (player == 1) {
            s1.text = Std.string(++scoreP1);
        } else {
            s2.text = Std.string(++scoreP2);
        }
        setGameStatus(Paused);
        
        // update scores
        if (player == 1) {
            sE.text = s1.text;
            sE.x = s1.x;
            sE.y = s1.y;
        }
        if (player == 2) {
            sE.text = s2.text;
            sE.x = s2.x;
            sE.y = s2.y;
        }
            
        // play score animation
        Actuate.tween (sE, 0.5, { alpha: 1 }).ease(Expo.easeOut).repeat(1).reflect();
        
        // also check if someone won
        Actuate.timer (1.2).onComplete(checkWinner);
    }
    
    private function checkWinner() {
        if (scoreP1 >= winCondition) {
            setGameStatus(Winner);
            cE.text = "Player 1 Wins!";
        }
        if (scoreP2 >= winCondition && gameMode == 1) {
            setGameStatus(Winner);
            cE.text = "Computer Wins!";
        }
        if (scoreP2 >= winCondition && gameMode == 2) {
            setGameStatus(Winner);
            cE.text = "Player 2 Wins!";
        }
        if (scoreP2 >= winCondition && gameMode == 99) { // just in case
            setGameStatus(Winner);
            cE.text = "Computer Wins!";
        }
        if (inGameStatus == Winner) { // if someone wonned
            // play animation
            Actuate.tween (cE, 0.4, { alpha: 1 }).ease(Expo.easeOut).repeat(5).reflect().onComplete(openEndGame);
        } else {
            // keep playing
            setGameStatus(Playing);
        }
    }
    
    // reset after scoring
    private function reset() {
        ballSpeed = ballSpeedDef;
        
        platform1.x = 0 + 64;
        platform1.y = 360 - (platform1.height / 2);
        
        platform2.x = 1280 - 64 - (platform2.width);
        platform2.y = 360 - (platform2.height / 2);
        
        ball.x = 640 - (ball.width / 2);
        ball.y = 360 - (ball.height / 2);
        
        // calculate the ball direction
        var direction:Int = (Math.random() > .5)?(1):( -1);
        var randomAngle:Float = (Math.random() * Math.PI / 2) - 45;
        ballMovement.x = direction * Math.cos(randomAngle) * ballSpeed;
        ballMovement.y = Math.sin(randomAngle) * ballSpeed;
        
        /*
        previousTime = 0;
        deltaTime = 0;
        initialTime = Lib.getTimer();
        cur = 0;
        
        trace ("ballSpeed: " + ballSpeed);
        */
        
        trace ("Game Reset");
    }
    
    /* Open Overlays */
    public function openModeSelect() {
        overlay = new ModeSelect();
        
        Lib.current.addChild(overlay); 
    }
    
    public function openPause() {
        previousTime = deltaTime;
        
        overlay = new Pause();
        
        Lib.current.addChild(overlay); 
    }
    
    private function openEndGame() {
        overlay = new EndGame();
        
        Lib.current.addChild(overlay); 
    }
    
    public function closeWindow() {
        Lib.current.removeChild(overlay); 
    }
    
}