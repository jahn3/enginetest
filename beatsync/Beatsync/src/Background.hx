package src;
import openfl.display.Sprite;

class Background extends Sprite
{

	public function new() 
	{
		super();
		this.graphics.beginFill(0x009193);
		this.graphics.drawRect(0, 0, 100, 100);
		this.graphics.endFill();
	}
}