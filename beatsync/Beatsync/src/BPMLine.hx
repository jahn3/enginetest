package;
import openfl.display.Sprite;

/**
 * ...
 * @author Chris Sanders
 */
class BPMLine extends Sprite
{
	public function new() 
	{
		super();
		this.graphics.beginFill(0x341515);
		this.graphics.drawRect(0, 0, 10, 1);
		this.graphics.endFill();
	}
}