package src;
import openfl.display.Sprite;

/**
 * ...
 * @author Chris Sanders
 */
class KeyRows extends Sprite
{

	public function new() 
	{
		super();
		this.graphics.beginFill(0xffffff);
		this.graphics.drawRect(0, 0, 100, 15);
		this.graphics.endFill();
	}
}