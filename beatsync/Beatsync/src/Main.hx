package;

import motion.MotionPath;
import openfl.display.Sprite;
import openfl.events.MouseEvent;
import openfl.geom.ColorTransform;
import openfl.Lib;
import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.text.TextField;
import src.Background;
import src.KeyRows;
import openfl.events.KeyboardEvent;
import openfl.Assets;
import openfl.media.Sound;
import openfl.media.SoundChannel;
import flash.events.TimerEvent;
import flash.utils.Timer;
import motion.Actuate;
import motion.easing.*;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

/**
 * ...
 * @author Chris Sanders
 */
class Main extends Sprite 
{
	
	private var bg:Background;
	
	//corresponding key rectangles
	private var keyLeft:KeyRows;
	private var keyUp:KeyRows;
	private var keyRight:KeyRows;
	private var keyDown:KeyRows;
	
	//boolean arrowkey assigning
	private var arrowKeyLeft:Bool;
	private var arrowKeyUp:Bool;
	private var arrowKeyRight:Bool;
	private var arrowKeyDown:Bool;
	
	//boolean increase/decrease bpm
	private var upArrowClick:Bool;
	private var downArrowClick:Bool;
	
	private var bpmLine:BPMLine;
	private var path:MotionPath;
	private var timer:Timer;
	private var setBPM:Int;
	private var tempo:Float;
	private var messageField:TextField;
	private var upArrow:TextField;
	private var downArrow:TextField;
	private var bpmNumber:TextField;
	private var newT:Float;
    
    private var colorTransform:ColorTransform;
    private var colorTransform1:ColorTransform;
    
    // BPM color effect
    private var bpmLineE:BPMLine;
    private var colorBPM:ColorTransform;
	
	var inited:Bool;

	/* ENTRY POINT */
	
	function resize(e) 
	{
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		if (inited) return;
		inited = true;

		//create background
		bg = new Background();
		
		//add event listeners for keypress
		stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		
		//set keys to false
		arrowKeyDown = false; 
		arrowKeyLeft = false;
		arrowKeyRight = false;
		arrowKeyUp = false;
		
		//1st input (left key)
		keyLeft = new KeyRows();
		keyLeft.x = 5;
		keyLeft.y = 450;
		this.addChild(keyLeft);
		
		//2nd input (up key)
		keyUp = new KeyRows();
		keyUp.x = 150;
		keyUp.y = 450;
		this.addChild(keyUp);
		
		//3rd input (right key)
		keyRight = new KeyRows();
		keyRight.x = 300;
		keyRight.y = 450;
		this.addChild(keyRight);
		
		//4th input (down key)
		keyDown = new KeyRows();
		keyDown.x = 450 ;
		keyDown.y = 450;
		this.addChild(keyDown);
		
		//create BPM instance
		bpmLine = new BPMLine();
		bpmLine.x = 300;
		bpmLine.y = 300;
		this.addChild(bpmLine);
        bpmLine.height = 100;
        
        //create BPM Line Effect
		bpmLineE = new BPMLine();
		bpmLineE.x = 300;
		bpmLineE.y = 300;
		this.addChild(bpmLineE);
        colorBPM = new ColorTransform();
		colorBPM.color = 0xaf4646;
        bpmLineE.alpha = 1;
        bpmLineE.height = 100;
        bpmLineE.transform.colorTransform = colorBPM;
		
		//add mouse event listeners
		stage.addEventListener(MouseEvent.MOUSE_DOWN, onClickDown, true);
		stage.addEventListener(MouseEvent.MOUSE_UP, onClickUp, false);
		
		//show change bpm text
		var messageFormat:TextFormat = new TextFormat("Verdana", 30, 0xffffff, false);
		messageFormat.align = TextFormatAlign.CENTER;
		messageField = new TextField();
		addChild(messageField);
		messageField.width = 250;
		messageField.defaultTextFormat = messageFormat;
		messageField.y = 100;
		messageField.selectable = false;
		messageField.text = "Change the BPM";
		
		//show increase arrow
		upArrow = new TextField();
		addChild(upArrow);
		upArrow.text = "▲";
		upArrow.y = 150;
		upArrow.defaultTextFormat = messageFormat;
		messageField.selectable = false;
		
		//show BPM text
		setBPM = 120;
		bpmNumber = new TextField();
		addChild(bpmNumber);
		bpmNumber.text = "" + setBPM;
		bpmNumber.y = 200;
		bpmNumber.defaultTextFormat = messageFormat;
		messageField.selectable = false;
		
		//show decrease arrow
		downArrow = new TextField();
		addChild(downArrow);
		downArrow.text = "▼";
		downArrow.y = 250;
		downArrow.defaultTextFormat = messageFormat;
		messageField.selectable = false;
		
		
		// bpm line effect
        getTempo();
        updateTween();
        
        //create color transformer to change input colors
		colorTransform = new ColorTransform();
		colorTransform1 = new ColorTransform();
		colorTransform.color = 0x69cbf5;
		colorTransform1.color = 0xffffff;
		
		//refresh window
		this.addEventListener(Event.ENTER_FRAME, everyFrame);
	}
	
	//so the tween can be called in the onClick function
	public function updateTween()
	{
        bpmLineE.alpha = 1;
		Actuate.tween (bpmLineE, tempo, { alpha: 0 } ).ease(Linear.easeNone).repeat();
	}
	
	// calculate tempo
	public function getTempo():Void
	{
		//set tempo math - 1 over whatever BPM is divided by 60 seconds
        tempo = (60 / setBPM) - (1 / 100);
	}

	//keydown for arrow keys
	public function onKeyDown(evt:KeyboardEvent):Void
	{

		if (evt.keyCode == 37)
		{
			arrowKeyLeft = true;
		}
		if (evt.keyCode == 38)
		{
			arrowKeyUp = true;
		}
		if (evt.keyCode == 39)
		{
			arrowKeyRight = true;
		}
		if (evt.keyCode == 40)
		{
			arrowKeyDown = true;
		}
	}
	
	//keyup for arrow keys
	public function onKeyUp(evt:KeyboardEvent):Void
	{
		if (evt.keyCode == 37)
		{
			arrowKeyLeft = false;
		}
		if (evt.keyCode == 38)
		{
			arrowKeyUp = false;
		}
		if (evt.keyCode == 39)
		{
			arrowKeyRight = false;
		}
		if (evt.keyCode == 40)
		{
			arrowKeyDown = false;
		}
	}
	
	public function onClickDown(evt:MouseEvent):Void
	{
		if (evt.target == upArrow)
		{
			upArrowClick = true;
		}
		if (evt.target == downArrow)
		{
			downArrowClick = true;
		}
		
	}
	public function onClickUp(evt:MouseEvent):Void
	{
		if (evt.target == upArrow)
		{
			upArrowClick = false;
			setBPM++;
			bpmNumber.text = Std.string(setBPM);
			getTempo();
			Actuate.stop (bpmLineE);
			updateTween();
		}
		if (evt.target == downArrow)
		{
			downArrowClick = false;
			setBPM--;
            bpmNumber.text = Std.string(setBPM);
			getTempo();
            Actuate.stop (bpmLineE);
			updateTween();
		}
	}
	
	//change the color of each frame on keydown
	private function everyFrame(event:Event):Void
	{
        
		//arrow keys
		if (arrowKeyLeft)
		{
			keyLeft.transform.colorTransform = colorTransform;
		} 
		else if (!arrowKeyLeft)
		{
			keyLeft.transform.colorTransform = colorTransform1;
		}
		if (arrowKeyUp)
		{
			keyUp.transform.colorTransform = colorTransform;
		}
		else if (!arrowKeyUp)
		{
			keyUp.transform.colorTransform = colorTransform1;
		}
		if (arrowKeyRight)
		{
			keyRight.transform.colorTransform = colorTransform;
		}
		else if (!arrowKeyRight)
		{
			keyRight.transform.colorTransform = colorTransform1;
		}
		if (arrowKeyDown)
		{
			keyDown.transform.colorTransform = colorTransform;
		}
		else if (!arrowKeyDown)
		{
			keyDown.transform.colorTransform = colorTransform1;
		}
		
		//bpm keys
		if (upArrowClick)
		{
			upArrow.transform.colorTransform = colorTransform;
		}
		if (!upArrowClick)
		{
			upArrow.transform.colorTransform = colorTransform1;			
		}
		if (downArrowClick)
		{
			downArrow.transform.colorTransform = colorTransform;
		}
		if (!downArrowClick)
		{
			downArrow.transform.colorTransform = colorTransform1;
		}
	}
	
	public function onTimer(e:TimerEvent):Void
	{
	}	


	/* SETUP */

	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, added);
	}

	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
	}

}
