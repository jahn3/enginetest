package ;

import openfl.display.Bitmap;
import openfl.display.Sprite;
import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.events.KeyboardEvent;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import openfl.Assets;
import openfl.media.Sound;
import openfl.media.SoundChannel;
import flash.events.TimerEvent;
import flash.utils.Timer;

enum Mode {
    Menu;
    Key;
    Sound;
    GFX;
}

enum Dialog {
    Wait;
    Running;
    Input;
}

enum SoundStatus {
    Paused;
    Playing;
}

class Main extends Sprite 
{   
    var inited:Bool;
    
    // keyboard input arrays
    private var keys:Array<Bool>;
    
    // text storage
    private var text:String;
    
    // dialog system
    private var dialogStatus:Dialog;
    private var dialogSpeed:Int;
    private var timerBlink:Timer;
    
    // dialog arrays & pointers
    private var dialogTest:Array<String>;
    private static var dialogTest_idx:Int;
    
    private var mainmenu:Array<String>;
    private static var mainmenu_idx:Int;
    
    private var keytest:Array<String>;
    private static var keytest_idx:Int;
    
    private var soundtest:Array<String>;
    private static var soundtest_idx:Int;
    
    private var gfxtest:Array<String>;
    private static var gfxtest_idx:Int;
    
    // dialog code
    private var dialogCode:String;
    
    // text fields
    private var messageField1:TextField;
    private var title:TextField;
    private var dialogWait:TextField;
    
    // sound
    private var soundtest1:Sound;
    private var soundtest2:Sound;
    private var channel:SoundChannel;
    private var soundStatus:SoundStatus;
    
    // graphics
    private var bg_monitor:Background;
    private var bg_screen:Screen;
    private var bg_input:Input;
    private var fg_cursor1:Cursor_Big;
    
    private var mode:Mode;
    
	/* ENTRY POINT */
	
	function resize(e) 
	{
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		if (inited) return;
		inited = true;
		
		// code
        
        /* KEYBOARD INPUT */
        
        // keyboard input arrays
        keys = [];
        
        // stage listener for key press
        stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
        
        /* GRAPHICS */
        
        // monitor bg
        bg_monitor = new Background();
        this.addChild(bg_monitor);
        
        // screen bg
        bg_screen = new Screen();
        bg_screen.x = 50;
        bg_screen.y = 25;
        this.addChild(bg_screen);
        
        // input bg
        bg_input = new Input();
        bg_input.x = 50;
        bg_input.y = 425;
        this.addChild(bg_input);
        
        /* TEXT FIELDS */
        
        // font load
        var fontName = Assets.getFont("fonts/Perfect_DOS_VGA_437.ttf").fontName;
        var DEFAULT_FORMAT:TextFormat = new TextFormat(fontName, 36, 0x666633, true);
        DEFAULT_FORMAT.align = TextFormatAlign.LEFT;
        
        // dialog text
        messageField1 = new TextField();
        addChild(messageField1);
        messageField1.width = 380;
        messageField1.height = 405;
        messageField1.x = 60;
        messageField1.y = 35;
        messageField1.defaultTextFormat = DEFAULT_FORMAT;
        messageField1.selectable = false;
        messageField1.text = "";
        messageField1.wordWrap = true;
        
        // dialog wait text
        dialogWait = new TextField();
        addChild(dialogWait);
        dialogWait.width = stage.stageWidth;
        dialogWait.x = 60;
        dialogWait.y = 435;
        dialogWait.defaultTextFormat = DEFAULT_FORMAT;
        dialogWait.selectable = false;
        dialogWait.text = "Press A...";
        
        // title text(not used)
        var messageFormat2:TextFormat = new TextFormat("Verdana", 72, 0xbbbbbb, true);
        messageFormat2.align = TextFormatAlign.CENTER;
        
        title = new TextField();
        addChild(title);
        title.width = stage.stageWidth;
        title.y = 100;
        title.defaultTextFormat = messageFormat2;
        title.selectable = false;
        title.text = "";
        
        /* DIALOGS */
        
        // dialog config
        dialogStatus = Wait;
        dialogSpeed = 30;
        
        // load dialogs
        mainmenu = openfl.Assets.getText("text/enginetest_main.dat").split("@@\n");
        mainmenu_idx = 0;
        
        keytest = openfl.Assets.getText("text/enginetest_key.dat").split("@@\n");
        keytest_idx = 0;
        
        soundtest = openfl.Assets.getText("text/enginetest_sound.dat").split("@@\n");
        soundtest_idx = 0;
        
        gfxtest = openfl.Assets.getText("text/enginetest_gfx.dat").split("@@\n");
        gfxtest_idx = 0;
        
        /* SOUNDS */
        
        // load
        soundtest1 = Assets.getSound("audio/kitty.wav");
        soundtest2 = Assets.getSound("audio/kitty.wav");
        soundStatus = Paused;
        
        /* EVENT LISTENERS */
        
        // every frame event listener
        this.addEventListener(Event.ENTER_FRAME, everyFrame);
        
        // blink event listener
        timerBlink = new Timer (480);
        timerBlink.addEventListener (TimerEvent.TIMER, timer_onTimer);
        timerBlink.start ();
        
        mode = Menu;
	}
    
    // blink
    private function timer_onTimer (event:TimerEvent):Void {
        if (dialogStatus == Wait) {
            dialogWait.alpha = (dialogWait.alpha == 0)?(1):(0);
        }
    }
    
    // every frame do something
    private function everyFrame(event:Event):Void {
        
        if (dialogStatus != Running) {
            // Menu
            if (mode == Menu) {
                if (dialogStatus == Wait) {
                    if (keys[65]) dialog(mainmenu[mainmenu_idx++]);
                    dialogWait.text = "Press A...";
                }
                if (dialogStatus == Input) {
                        if (keys[65]) { dialog(keytest[keytest_idx]); mode = Key; }
                        if (keys[83]) { dialog(soundtest[soundtest_idx]); mode = Sound; }
                        if (keys[68]) { dialog("0DialogTest \n"); mode = GFX; }
                }
            }

            // KeyTest
            if (mode == Key) {
                if (dialogStatus == Wait) {
                    dialogWait.text = "Press A...";
                }
                if (dialogStatus == Input) {
                    stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
                    stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
                    dialogWait.text = "Esc to Back";
                    dialogWait.alpha = 1;
                }
            }
                
            // SoundTest
            if (mode == Sound) {
                if (dialogStatus == Wait) {
                    dialogWait.text = "Press A...";
                }
                if (dialogStatus == Input) {
                    if (keys[65]) {
                        dialogStatus = Running;
                        if (soundStatus == Playing) {
                            channel.stop();
                            messageField1.text = "SoundTest\n\nA: Play WAV \nS: Play OGG";
                            soundStatus = Paused;
                        } else {
                        channel = soundtest1.play();
                        messageField1.text = "SoundTest\n\nA: Play WAV <<<\nS: Play OGG";
                        soundStatus = Playing;
                        }
                        haxe.Timer.delay(dialogInput.bind(), 1000);
                    }
                    if (keys[83]) {
                        dialogStatus = Running;
                        if (soundStatus == Playing) {
                            channel.stop();
                            messageField1.text = "SoundTest\n\nA: Play WAV \nS: Play OGG";
                            soundStatus = Paused;
                        } else {
                        channel = soundtest2.play();
                        messageField1.text = "SoundTest\n\nA: Play WAV \nS: Play OGG <<<";
                        soundStatus = Playing;
                        }
                        haxe.Timer.delay(dialogInput.bind(), 1000);
                    }
                    dialogWait.text = "Esc to Back";
                    dialogWait.alpha = 1;
                }
            }
                
            // DialogTest
            if (mode == GFX) {
                if (dialogStatus == Wait) {
                    if (keys[65]) dialog(gfxtest[gfxtest_idx++]);
                    dialogWait.text = "Press A...";
                }
            }
                    
            // escape
            if (keys[27]) { dialog(mainmenu[mainmenu_idx++]); mode = Menu; }
        }
        
        // dont blink while running
        if (dialogStatus == Running) dialogWait.alpha = 0;
            
        // reset indexes
        mainmenu_idx = (mainmenu_idx != mainmenu.length-1)?(mainmenu_idx):(0);
        keytest_idx = (keytest_idx != keytest.length-1)?(keytest_idx):(0);
        soundtest_idx = (soundtest_idx != soundtest.length-1)?(soundtest_idx):(0);
        gfxtest_idx = (gfxtest_idx != gfxtest.length-1)?(gfxtest_idx):(0);
    }
    
    // on key down
    private function onKeyDown(evt:KeyboardEvent):Void {
	    keys[evt.keyCode] = true;
        if (mode == Key && dialogStatus != Running) messageField1.text = "KeyTest\n\nChar code: " + evt.charCode + "\nKey code: " + evt.keyCode;
    }
    
    // on key up
    private function onKeyUp(evt:KeyboardEvent):Void {
	    keys[evt.keyCode] = false;
        if (mode == Key && dialogStatus != Running) messageField1.text = "KeyTest\n\nChar code: \nKey code: ";
    }
    
    // dialog system
    private function dialog(textInput:String) {
        dialogStatus = Running;
        text = textInput;
        
        var i = 1;
        var d = 0;
        
        while (i < text.length+1) {
             d = dialogSpeed*i;
             haxe.Timer.delay(printDialog.bind(text, i++), d);
        }
        
        dialogCode = text.substr(0, 1);
        
        if (dialogCode == "0" || dialogCode == "NULL") haxe.Timer.delay(dialogNormal.bind(), d+dialogSpeed); // Normal Text
        if (dialogCode == "4") haxe.Timer.delay(dialogInput.bind(), d+dialogSpeed); // Input Required
    }
    
    // put dialogStatus into Wait when all the text has been outputed
    private function dialogNormal() {
        dialogStatus = Wait;
    }
    
    private function dialogInput() {
        dialogStatus = Input;
    }
    
    // dialog system print
    private function printDialog(textFull:String, index:Int):Void {
        messageField1.text = text.substr(1, index);
    }
    
	/* SETUP (DO NOT TOUCH) */

	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, added);
	}

	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
		//
	}
}