package ;
import openfl.display.Sprite;

class Background extends Sprite
{

	public function new() 
	{
		super();
		this.graphics.beginFill(0x009193);
		this.graphics.drawRect(0, 0, 500, 500);
		this.graphics.endFill();
	}
}