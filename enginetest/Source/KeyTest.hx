package ;

import flash.display.Sprite;
import flash.events.Event;
import flash.Lib;
import openfl.events.KeyboardEvent;
import openfl.text.TextField;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;

class KeyTest extends Sprite 
{   
    var inited:Bool;
    private var keys:Array<Bool>;
    private var messageField1:TextField;
    private var messageField2:TextField;
    private var title:TextField;

	/* ENTRY POINT */
	
	function resize(e) 
	{
		if (!inited) init();
		// else (resize or orientation change)
	}
	
	function init() 
	{
		if (inited) return;
		inited = true;
		
		// code
        
        // key arrays
        keys = [];
        
        // stage listener for key press
        stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
        stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
            
        var messageFormat:TextFormat = new TextFormat("Verdana", 18, 0xbbbbbb, true);
        messageFormat.align = TextFormatAlign.CENTER;

        messageField1 = new TextField();
        addChild(messageField1);
        messageField1.width = stage.stageWidth;
        messageField1.y = stage.stageHeight - 100;
        messageField1.defaultTextFormat = messageFormat;
        messageField1.selectable = false;
        
        messageField2 = new TextField();
        addChild(messageField2);
        messageField2.width = stage.stageWidth;
        messageField2.y = stage.stageHeight - 50;
        messageField2.defaultTextFormat = messageFormat;
        messageField2.selectable = false;
        
        var messageFormat2:TextFormat = new TextFormat("Verdana", 72, 0xbbbbbb, true);
        messageFormat2.align = TextFormatAlign.CENTER;
        
        title = new TextField();
        addChild(title);
        title.width = stage.stageWidth;
        title.y = 100;
        title.defaultTextFormat = messageFormat2;
        title.selectable = false;
        title.text = "KeyTest";
	}
    
    private function onKeyDown(evt:KeyboardEvent):Void {
	    keys[evt.keyCode] = true;
        messageField1.text = "Char code: \n" + evt.charCode;
        messageField2.text = "Key code: \n" + evt.keyCode;
    }

    private function onKeyUp(evt:KeyboardEvent):Void {
        keys[evt.keyCode] = false;
        messageField1.text = "Char code: \n ";
        messageField2.text = "Key code: \n ";
    }

	/* SETUP */

	public function new() 
	{
		super();	
		addEventListener(Event.ADDED_TO_STAGE, added);
	}

	function added(e) 
	{
		removeEventListener(Event.ADDED_TO_STAGE, added);
		stage.addEventListener(Event.RESIZE, resize);
		#if ios
		haxe.Timer.delay(init, 100); // iOS 6
		#else
		init();
		#end
	}
	
	public static function main() 
	{
		// static entry point
		Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		Lib.current.addChild(new Main());
		//
	}
}